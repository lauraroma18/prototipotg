//Clase Celda --> almacena la información de las celdas vecinas de su entorno
public class Celda{
  int vecinos;
  int depredadores;
  int vacias;
  int x;
  int y;
  int tipo;
  int emocion;
  
  public Celda(int x, int y, int tipo, int vecinos, int depredadores, int vacias, int emocion){
    this.vecinos = vecinos;
    this.depredadores = depredadores;
    this.vacias = vacias;
    this.x = x;
    this.y = y;
    this.tipo = tipo;
    this.emocion = emocion;
  }
  
  public int getx(){
    return x;
  }
  
  public int gety(){
    return y;
  }
  
  public int getTipo(){
    return tipo;
  }
  public int getvecinos(){
    return vecinos;
  }
  
   public int getdepredadores(){
    return depredadores;
  }
  
   public int getvacias(){
    return vacias;
  }
  
  public int getemocion(){
    return emocion;
  }
}