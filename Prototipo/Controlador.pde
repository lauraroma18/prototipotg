import java.util.ArrayList;
import java.util.Random;
import java.lang.Integer;

import java.io.BufferedReader;
import java.io.FileReader;

import java.io.FileNotFoundException;
import java.io.IOException;

//Clase Controlador
public class Controlador {
  //Array bidimensional para los NPC
  CA [][] automatas;
  int dimension;

  int generacion = 0;

  //BOTON PAUSA
  int bpausax = width-160;
  int bpausay = height/3;
  int bpausaRadio = 70;
  boolean estadoPausa = false;

  //Objeto de tipo fuente (tipografia)
  PFont font;
  PFont fontB;

  //imagen
  PImage fondo;

  //dimensiones de la grilla
  int anchoGrilla;
  int altoGrilla;

  // tiempo entre generaciones (segundos)
  int tiempoGeneraciones;

  //depredadores maximos por generación
  int depredadoresMax;

  //vida del depredador (por generaciones)
  int vidaDepredador;

  // Taza de generación de depredadores (por generaciones)
  int tazaDepredador;

  // Indica cual pantalla mostrar
  int auxPantalla=0;

  // Almacena los valores digitados
  String valores[] = new String[1];  

  public Controlador() {

    font = loadFont("DialogInput.bold-48.vlw");
    fontB = loadFont("DialogInput.plain-48.vlw");
    fondo = loadImage("fondo.jpg");

    //Configuración predeterminada
    anchoGrilla = 7;
    altoGrilla = 7;
    tiempoGeneraciones = 3;
    depredadoresMax = 2;
    tazaDepredador = 2;

    anchoGrilla = 7;
    altoGrilla = 7;
  }

  //Muestra la configuración o el tablero como tal
  public void show() {
    image(fondo, 0, 0, width, height); 

    switch(auxPantalla) {
    case 0:
      pantallaInicial();
      break;

    case 1:
      pantallaPrototipo();
      crearDepredador();
      break;
    }
  }

  //metodo panel de configuración

  public void pantallaInicial() {

    textAlign(CENTER);
    textFont(font);
    text("Panel de Configuración", (width/2), 60);

    textSize(30);
    text("Tamaño de la Grilla", (width/2), 150);
    rectMode(CENTER);
    fill(255);
    rect(width/2, 200, 70, 30);
    fill(0);
    if ((valores[0] != null)) {
      text(valores[0], width/2, 210);
    }    

    // Botón Iniciar
    ellipseMode(RADIUS);
    fill(174, 176, 179);
    ellipse(width/2, height/2+50, bpausaRadio, bpausaRadio);
    fill(255);
    textSize(20);
    text("Iniciar", width/2, height/2+50);
  }


  //metodo pantalla de simulación
  public void pantallaPrototipo() {

    //Muestra la generación arriba de la grilla
    String gen = "Generación "+generacion;
    textAlign(CENTER);
    textFont(font);
    text(gen, (width/2), 60);

    // Botón PAUSA
    ellipseMode(RADIUS);
    fill(255, 20, 0);
    ellipse(bpausax, bpausay, bpausaRadio, bpausaRadio);
    fill(255);
    textSize(20);
    text("PAUSAR", bpausax, bpausay+5);

    /* DETALLAR COLORES EN PANTALLA
     0 : Nervioso : Color Verde    (30, 201, 30)
     1 : Calmado  : Color Naranja  (255, 143, 24)  
     2 : Triste   : Color Azul     (45, 121, 241)
     3 : Feliz    : Color amarillo (255, 235, 38)
     -1: SinEmocion : Color Gris   (174, 176, 179)
     */
    textSize(15);
    rectMode(CENTER);
    textAlign(LEFT);

    //nervios
    fill(30, 201, 30) ;
    rect(40, (height/2)-80, 25, 25);
    fill(255);
    text("Nervioso", 75, (height/2)-80+5);

    //calmado
    fill(255, 143, 24) ;
    rect(40, (height/2)-40, 25, 25);
    fill(255);
    text("Calmado", 75, (height/2)-40+5);

    //triste
    fill(45, 121, 241);
    rect(40, height/2, 25, 25);
    fill(255);
    text("Triste", 75, (height/2)+5);

    //Feliz
    fill(255, 235, 38);
    rect(40, (height/2)+40, 25, 25);
    fill(255);
    text("Feliz", 75, (height/2)+40+5);

    //Sin emoción
    fill(174, 176, 179);
    rect(40, (height/2)+80, 25, 25);
    fill(255);
    text("Sin emoción", 75, (height/2)+80+5);

    //Depredador
    fill(243, 56, 40);
    rect(40, (height/2)+120, 25, 25);
    fill(255);
    text("Depredador", 75, (height/2)+120+5);


    pintar();
    //asesinar(automatas,3,3);
    delay(5000);
    LOD();
    //println("************************************************* Siguiente Generacion");
    // println(vecinosVivos(5,0,6));
    generacion ++;
  }

  public void crearAutomatas() {
    automatas = new CA[dimension][dimension];
    int ancho = 600/dimension;
    int x = (width/4) + (ancho/2);
    int ejey = 109 + (ancho/2);
    //rectMode(CENTER);
    for (int i=0; i<dimension; i++) {
      for (int k=0; k<dimension; k++) {
        CA automata = new CA(0, x, ejey);
        automatas[i][k] = automata;
        x += ancho;
      }
      ejey += ancho;
      x= (width/4) + (ancho/2);
    }
  }

  //configuración inicial  del tablero.
  public void tableroinicial() {
    int cantDepredadores = dimension/4;
    int cantAutomatas = dimension*dimension/2;
    int x;
    int y;
    Random random = new Random(System.currentTimeMillis());

    //Crear depredadores
    for (int i=0; i<cantDepredadores; i++) {
      automatas[random.nextInt(dimension)][random.nextInt(dimension)].setTipo(2);
    }

    for (int i=0; i<cantAutomatas; i++) {
      x = random.nextInt(dimension);
      y = random.nextInt(dimension);
      automatas[x][y].setTipo(1);
      automatas[x][y].setemocion(3);
    }


    /*Crear Automatas
     automatas[3][3].setTipo(2);
     
     automatas[2][3].setTipo(1);
     automatas[2][3].setemocion(3);
     
     automatas[3][4].setTipo(1);
     automatas[3][4].setemocion(3);
     
     automatas[4][2].setTipo(1);
     automatas[4][2].setemocion(3);
     
     automatas[4][3].setTipo(1);
     automatas[4][3].setemocion(3);
     
     automatas[4][4].setTipo(1);
     automatas[4][4].setemocion(3);
     */
  }

  /* Pinta los automatas en las celdas correspondientes a su posición   
   0 : Nervioso : Color Verde    (30, 201, 30)
   1 : Calmado  : Color Naranja  (255, 143, 24)  
   2 : Triste   : Color Azul     (45, 121, 241)
   3 : Feliz    : Color amarillo (255, 235, 38)
   -1: SinEmocion : Color Gris   (174, 176, 179)
   */

  public void pintar() {
    int ancho = 600/dimension;
    int x;
    int y;
    rectMode(CENTER);

    for (int i=0; i<dimension; i++) {
      //ArrayList <CA> automatasColumnas = automatasFilas.get(i);
      for (int k=0; k<dimension; k++) {
        x=automatas[i][k].getposx();
        y=automatas[i][k].getposy();
        //println(automatas[i][k].getposx());
        //Se asigna un color para pintar dependiendo si esta vivo (azul), muerto (Blanco), Depredador (Rojo).
        if (automatas[i][k].getTipo()==1) {
          if (automatas[i][k].getemocion()==0) { //nervios
            fill(30, 201, 30);
          } else if (automatas[i][k].getemocion()==1) { // Calmado
            fill (255, 143, 24);
          } else if (automatas[i][k].getemocion()==2) {// triste
            fill(45, 121, 241);
          } else if (automatas[i][k].getemocion()==3) { // Feliz
            fill(255, 235, 38);
          } else {
            fill(174, 176, 179);
          }
        } else if (automatas[i][k].getTipo()==2) {

          fill(243, 56, 40);
        } else { 
          fill(255, 255, 255);
        }
        rect(x, y, ancho, ancho);
      }
    }
  }

  //Cuenta la cantidad de celdad vecinas con un tipo específico de autómata
  public int contarTiposVecinos(CA[][] copy, int arrayX, int arrayY, int tipo) {
    int cantidad =0;
    int x;
    int y;

    for (int row=-1; row<2; row++) {
      for (int col=-1; col<2; col++) {
        x = arrayX + row;
        y = arrayY + col;
        if (x<0 || x>=dimension || y<0 || y>=dimension || (row==0 && col==0)) {
          cantidad += 0;
        } else {
          if (copy[x][y].getTipo()==tipo) {   
            cantidad ++;
          }
        }
      }//end for 2
    }//end for 1
    return cantidad;
  }

  public void LOD() {
    int estado= 0;
    //almacena la info de un celda especifica
    Celda infocelda;

    //ArrayList con la información de cada celda en la generación actual
    Celda[][] genactual = new Celda [dimension][dimension];

    //Construir la matriz de generación actual para poder modificar en el tablero de automatas la siguiente generacón
    for (int i=0; i<dimension; i++) {
      for (int k=0; k<dimension; k++) {
        infocelda = new Celda(i, k, automatas[i][k].getTipo(), contarTiposVecinos(automatas, i, k, 1), contarTiposVecinos(automatas, i, k, 2), contarTiposVecinos(automatas, i, k, 0), automatas[i][k].getemocion());
        genactual[i][k] = infocelda;
      }//end for 2
    }//end for 1
    /* MOSTRAR TIPO DE GENACTUAL
     for(int i=0; i<dimension; i++){
     for(int k=0; k<dimension; k++){
     
     println( genactual[i][k].getTipo()); 
     
     }//end for 2
     }//end for 1
     println("****************++");*/

    //Modificamos la matriz de autómatas para la siguiente generación.
    for (int i=0; i<dimension; i++) {
      for (int k=0; k<dimension; k++) {

        estado= automatas[i][k].getTipo();

        if ((automatas[i][k].getTipo()==1) && (genactual[i][k].getvecinos()<2 || genactual[i][k].getvecinos()>3)) {

          //DIE!
          automatas[i][k].setTipo(0);
          automatas[i][k].setemocion(-1);
        } else if ((automatas[i][k].getTipo()==0) && genactual[i][k].getvecinos()==3) {
          padres(genactual, i, k);
          //automatas[i][k].setTipo(1);
          calcularemocion(i, k, genactual[i][k].getvecinos(), genactual[i][k].getdepredadores(), genactual[i][k].getvacias());
        } else if (automatas[i][k].getTipo()==2) {    

          automatas[i][k].disminuirTiempovida();
          asesinar(genactual, i, k);
        } else {

          if (automatas[i][k].getTipo()==1) {
            calcularemocion(i, k, genactual[i][k].getvecinos(), genactual[i][k].getdepredadores(), genactual[i][k].getvacias());
          }         
          automatas[i][k].setTipo(estado);
        }//Le reasignamos el estado que tiene 

        //println(automatas[i][k].getemocion());
      }// end for 2
    }// end for 1
  }// end LOD

  public void calcularemocion(int posx, int posy, int vecinos, int depredadores, int vacias) {
    int emotion;
    int ocupadas = vecinos + depredadores;
    float placer;
    float activacion;
    activacion = (vacias * automatas[posx][posy].getinfluenciapositiva()) - (ocupadas * automatas[posx][posy].getinfluencianegativa());

    placer = (vecinos * automatas[posx][posy].getinfluenciapositiva()) - ((depredadores * 2) * automatas[posx][posy].getinfluencianegativa());
    /*
   println(" Informacion automata");
     println("Pos x: " + posx + " Pos y: " + posy + " Activación: " + activacion + " Placer: " + placer + " Vacias: " + vacias + " Ocupadas: " + vecinos);
     println("Influencia Negativa: " + automatas[posx][posy].getinfluencianegativa() + " Influjencia Positiva: " + automatas[posx][posy].getinfluenciapositiva());
     println("***************");
     println();
     */

    if (activacion>0 && placer>0) {
      // feliz
      emotion = 3;
    } else if (activacion>0 && placer<0) {
      // nevioso
      emotion = 0;
    } else if (activacion<0 && placer>0) {
      // calmado
      emotion = 1;
    } else if (activacion<0 && placer<0) {
      // triste
      emotion = 2;
    } else { 
      emotion = -1; // Devoto del creador (Rezando el rosario)
    }

    automatas[posx][posy].setemocion(emotion);
  }

  public boolean compatibilidad(Celda[][] copy, int padrex, int padrey, int madrex, int madrey, int existedepredador) { //existedepredador--> 0=NO 1=SI
    int emocionpadre = copy[padrex][padrey].getemocion();
    int emocionmadre = copy[madrex][madrey].getemocion();

    float positivapadre = automatas[padrex][padrey].getinfluenciapositiva();
    float positivamadre = automatas[madrex][madrey].getinfluenciapositiva();

    float negativapadre = automatas[padrex][padrey].getinfluencianegativa();
    float negativamadre = automatas[madrex][madrey].getinfluencianegativa();

    float personalidadmadre = positivamadre + negativamadre;
    float personalidadpadre = positivapadre + negativapadre;

    float rango;
    float lsuperior;
    float linferior;



    switch(emocionpadre) {

    case -1: 
      if (emocionmadre == 2) { // feliz-triste
        return false;
      } else if (emocionmadre == 3) {
        return true;
      } else if ((emocionmadre == 0) ) { //  feliz-nervioso  
        linferior = personalidadpadre-0.05;
        lsuperior = personalidadpadre+0.05;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      } else if ((emocionmadre == 1) ) { //   feliz-calmado  
        linferior = personalidadpadre-0.10;
        lsuperior = personalidadpadre+0.10;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      }

    case 0:
      rango = 0.05;
      if (existedepredador==0) {

        if (emocionmadre == 2) { // nervioso-triste
          return false;
        } else if ((emocionmadre == 0) || (emocionmadre == 1) || (emocionmadre == 3)) { //nervioso-nervioso  nervioso-calmado nervioso-feliz
          linferior = personalidadpadre-rango;
          lsuperior = personalidadpadre+rango;
          if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
            return true;
          } else {
            return false;
          }
        }
      }
    case 1:
      rango = 0.10;
      if (emocionmadre == 2) { // calmado-triste
        return false;
      } else if ((emocionmadre == 0) ) { //calmado-nervioso  
        linferior = personalidadpadre-0.05;
        lsuperior = personalidadpadre+0.05;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      } else if ((emocionmadre == 1) || (emocionmadre == 3)) { //calmado-calmado  calmado-feliz
        linferior = personalidadpadre-rango;
        lsuperior = personalidadpadre+rango;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      }
    case 2:  // triste-x
      return false;

    case 3: 
      if (emocionmadre == 2) { // feliz-triste
        return false;
      } else if (emocionmadre == 3) {
        return true;
      } else if ((emocionmadre == 0) ) { //  feliz-nervioso  
        linferior = personalidadpadre-0.05;
        lsuperior = personalidadpadre+0.05;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      } else if ((emocionmadre == 1) ) { //   feliz-calmado  
        linferior = personalidadpadre-0.10;
        lsuperior = personalidadpadre+0.10;
        if ( (personalidadmadre >= linferior) && (personalidadmadre <= lsuperior) ) {
          return true;
        } else {
          return false;
        }
      }
    default:
      return false;
    }
  } 

  public void padres(Celda[][] copy, int arrayX, int arrayY) {
    ArrayList<int[]> vecinos;   
    vecinos = arrayvecinos(copy, arrayX, arrayY); //Se obtienen las posiciones de los los automatas que pueden ser padres
    boolean pareja;

    /*
   println("Posibles padres");
     
     for (int i = 0; i<vecinos.size(); i++){
     println(vecinos.get(i)[0] +" | " + vecinos.get(i)[1] + " Emocion: " + automatas[vecinos.get(i)[0]][vecinos.get(i)[1]].getemocion());
     }
     */
    for (int i=0; i<2; i++) {
      for (int j=i+1; j<=2; j++) {
        pareja =  compatibilidad(copy, vecinos.get(i)[0], vecinos.get(i)[1], vecinos.get(j)[0], vecinos.get(j)[1], copy[arrayX][arrayY].getdepredadores());
        if (i!=j) {
          if (pareja) {
            //println("Nacer Pos x: " + arrayX + " Pos y: " + arrayY + " Padre: " + vecinos.get(0)[0] + ":" + vecinos.get(0)[1] +  " Madre: " + vecinos.get(1)[0] + ":" + vecinos.get(1)[1]);
            automatas[arrayX][arrayY].setTipo(1);
            break;
          }
        }
      }
    }
  }

  // Función donde el depredador mata a uno de sus vecinos
  public void asesinar(Celda[][] copy, int arrayX, int arrayY) {
    ArrayList<int[]> victimas = new ArrayList<int[]>();

    int x;
    int y;
    int matar;


    victimas = arrayvecinos(copy, arrayX, arrayY);

    /*
   for(int i=0; i<victimas.size(); i++){
     println("--vecino "+i);
     println("x: "+victimas.get(i)[0]);
     println("y: "+victimas.get(i)[1]);
     
     }
     
     println("*******************");
     */

    if (victimas.size()>0) {
      matar = aleatorio(victimas.size());
      x = victimas.get(matar)[0];
      y = victimas.get(matar)[1];
      //println("Muere X:  "+x+" Y: "+y);
      automatas[x][y].setTipo(0);
    }
  }

  //
  public ArrayList arrayvecinos(Celda[][] copy, int arrayX, int arrayY) {
    int x;
    int y;
    ArrayList<int[]> vecinosvivos = new ArrayList<int[]>();
    int[] vivo; 

    for (int row=-1; row<2; row++) {
      for (int col=-1; col<2; col++) {
        x = arrayX + row;
        y = arrayY + col;
        // println("x:"+x+"y:"+y+" dimension:"+dimension);
        if ((x>=0) && (x<dimension) && (y>=0) && (y<dimension) && (copy[x][y].getTipo()==1)) {
          if ((x != arrayX) || (y!=arrayY)) {
            //println("entré al if");
            vivo = new int[2];
            vivo[0]= x;
            vivo[1]= y;
            vecinosvivos.add(vivo);
          }
        }
      }//end for 2
    }//end for 1*/
    //println(vecinosvivos.size());

    return vecinosvivos;
  }

  // Genera un número aleatorio entre 0 y el límite.
  public int aleatorio(int limite) {
    Random random = new Random(System.currentTimeMillis());

    return random.nextInt(limite);
  }

  public void mouseClicked() {
    //Boton PAUSA
    if ( dist(mouseX, mouseY, bpausax, bpausay)< bpausaRadio && auxPantalla==1) {
      if (!estadoPausa) {
        estadoPausa = true;
        ellipseMode(RADIUS);
        fill(6, 192, 7);
        ellipse(bpausax, bpausay, bpausaRadio, bpausaRadio);
        fill(255);
        textSize(20);
        text("REANUDAR", bpausax-45, bpausay+5);
        noLoop();
      } else {
        estadoPausa = false;
        loop();
      }
    }

    //Boton Iniciar, Si la grilla no se ha especificado se despliega una ventana para seleccionar el archivo txt con el tablero inicial
    if ( dist(mouseX, mouseY, width/2, height/2+50) < bpausaRadio && auxPantalla==0) {
      if (valores[0] == null) {
        //Una vez seleccionado el archivo se ejecuta la funcion fileSelected
        selectInput("Select a file to process:", "fileSelected", null, this);
      } else {
        valores[0] = valores[0].substring(0, valores[0].length()-1);
        dimension = Integer.parseInt(valores[0]);
        crearAutomatas();
        tableroinicial();
        auxPantalla = 1;
      }
      //Despues de asignar el tamaño de la dimension se crea el tablero de automatas y se agregan unos por defecto en base al tamaño de la grilla
    }
  }

  //La informacion del archivo seleccionado se copia en un arrayList para actualizar los datos de la matriz CA automatas
  void fileSelected(File selection) {
    BufferedReader reader;
    ArrayList<ArrayList> listaValores = new ArrayList<ArrayList>();
    String cadena;

    if (selection == null) {
      println("Window was closed or the user hit cancel.");
    } else {
      //println("User selected " + selection.getAbsolutePath());
      reader = createReader(selection);

      try {
        while ((cadena = reader.readLine())!=null) {

          String[] values = cadena.split(" ");
          ArrayList<Integer> a = new ArrayList<Integer>();
          for (int i = 0; i<values.length; i++) {
            a.add(Integer.parseInt(values[i]));
          }
          listaValores.add(a);
        }
        reader.close();
      } 
      catch (IOException e) {
        e.printStackTrace();
      }
    }

    dimension = listaValores.size();

    //la matriz automatas se inicializa con objetos de la clase CA
    crearAutomatas();
    for (int i=0; i<listaValores.size(); i++) {
      ArrayList aux = new ArrayList();
      aux = listaValores.get(i);
      for (int j=0; j<listaValores.size(); j++) {
        //println("x: " + i + " y: " + j + " Valor: " + aux.get(j));
        int type = (int) aux.get(j);
        
        //El valor 4 es un depredador
        if (type==4) {
          automatas[i][j].setTipo(2);
        }
        
        //El valor 5 es una celda vacia (automata muerto)
        else if(type==5){
          //automatas[i][j].setTipo(0);
        }
        
        //Si el valor no entra en las condiciones anteriores significa que es un automata vivo y se le asigna la emocion especificada
        // Nervioso = 0 : Calmado = 1 : Triste = 2 : Feliz = 3 : Sin emocion = -1
        else{
           automatas[i][j].setTipo(1);
           automatas[i][j].setemocion(type);
        }
      }
    }
    auxPantalla = 1;
  }

  /* El simbolo "|" permite guardar mas de un valor*/
  public void keyPressed() {

    //Se valida que ya esté algun valor almacenado
    if (key == BACKSPACE && valores[0].length() > 1) {
      //Se borra el ultimo valor introducido
      valores[0] = valores[0].substring(0, valores[0].length()-2)+"|";
      //cambia de posicion con la tecla tab
    } else if ((key>='0') && (key<='9')) {
      if (valores[0] == null) {
        //Si no hay ningun valor, se agrega el key + el simbolo |
        valores[0] = key+"|";
      }
      // El numero maximo permitido es de 3 cifras
      else if (valores[0].length()<4) {
        valores[0] = valores[0].substring(0, valores[0].length()-1)+key+"|";
      }
    }
  }

  public void crearDepredador() {
    int x;
    int y;
    boolean stop= false;
    Random random = new Random(System.currentTimeMillis());

    if (generacion%5 == 0) {
      for (int i=0; i<(dimension/5); i++) {
        do {
          x = random.nextInt(dimension);
          y = random.nextInt(dimension);
          if (automatas[x][y].getTipo() == 0) {
            automatas[x][y].setTipo(2);
            stop=true;
          }
        } while (!stop);
      }
    };
  }
}